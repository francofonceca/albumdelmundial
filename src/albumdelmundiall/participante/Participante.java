package src.albumdelmundiall.participante;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import src.albumdelmundiall.figurita.FiguritaTop10;
import src.albumdelmundiall.figurita.FiguritaTradicional;
import src.albumdelmundiall.album.Album;
import src.albumdelmundiall.album.AlbumExtendido;
import src.albumdelmundiall.album.AlbumTradicional;
import src.albumdelmundiall.album.AlbumWeb;

public class Participante {
	private int dni;
	private String nombre;
	private Album album;
	private HashMap<Integer, FiguritaTradicional> figuritasTradicionales;
	
	public Participante(int dni, Album album, String nombre) {
		this.dni = dni;
		this.nombre = nombre;
		this.album = album;
		this.figuritasTradicionales = new HashMap<>();
	}

	public void cargarSobreDeFiguritas(List<FiguritaTradicional> sobre) {

		for (FiguritaTradicional f : sobre) {
			figuritasTradicionales.put(f.getCodigo(), f);
		}
	}

	public HashMap<Integer, FiguritaTradicional> pegarFiguritas() {
		HashMap<Integer, FiguritaTradicional> pegadas = new HashMap<>();
		figuritasTradicionales.forEach((codigo, figurita) -> {

			if (album.verTipoAlbum().equals("Extendido") && figurita.getTipo().equals("Top10")) {

				AlbumExtendido extendido = (AlbumExtendido) album;

				FiguritaTop10 figutop10 = (FiguritaTop10) figurita;

				extendido.pegarFiguritaTop10(figutop10);

				pegadas.put(figurita.getCodigo(), figurita);

			}

			if (figurita.getTipo().equals("Tradicional") && !album.sePegoFigurita(figurita)) {

				album.pegarFigurita(figurita);
				pegadas.put(figurita.getCodigo(), figurita);
			}

		});

		return pegadas;
	
	}
	
	public int darCodigoFiguRepetida() {	
		if (figuritasTradicionales.isEmpty()) {
			return -1;
		}
		
		for (Map.Entry<Integer, FiguritaTradicional> figu : figuritasTradicionales.entrySet()) {
			return figu.getValue().getCodigo();
		}

		return -1;

	}
	
	public boolean tieneEsaFigurita(int cod) {
		if (cod == -1) {
			throw new RuntimeException("codigo invalido");
		}
		if (figuritasTradicionales.size() == 0) {
			return false;
		}

		if (figuritasTradicionales.get(cod) != null) {
			return true;
		}
		return false;
	}

	public boolean utilizoCodigo() {
		return ( (AlbumWeb) album).utilizoCodigo(); 
	}
	
	public FiguritaTradicional obtenerFiguritaTradicional(Integer codFiguritaTradicional) {
		return figuritasTradicionales.get(codFiguritaTradicional);
	}

	public void intercambiar(FiguritaTradicional FiguritaTradicional, FiguritaTradicional nuevaFiguritaTradicional) {
		figuritasTradicionales.remove(FiguritaTradicional.getCodigo());
		figuritasTradicionales.put(nuevaFiguritaTradicional.getCodigo(), nuevaFiguritaTradicional);
	}
	
	public void usarSorteo() {
		if (album.verTipoAlbum().equals("Tradicional")) { 
			AlbumTradicional trad = (AlbumTradicional) album;
			if (trad.seLeSorteoPremio()) {
				throw new RuntimeException("Ya se le sorteo un premio");
			}
			trad.sortearPremio();
		} else throw new RuntimeException ("No es posible sortear porque no es un album tradicional");
	}
	
	public boolean seUtilizoSorteo() {
		if (album.verTipoAlbum().equals("Tradicional")) 
			return ((AlbumTradicional) album).seLeSorteoPremio();
		return false;
	}

	public boolean seCompletoAlbum() {
		return album.seCompletoAlbum();
	}

	public String obtenerPremio() {
		return album.obtenerPremio();
	}

	public boolean seCompletoSeleccion(String seleccion) {
		return album.seCompletoSeleccion(seleccion);
	}

	public String obtenerNombre() {
		return nombre;
	}
	
	public int obtenerDNI() {
		return dni;
	}

	public String obtenerTipoAlbum() {
		return album.verTipoAlbum();
	}
	
	public boolean tieneFiguritas() {
		return !figuritasTradicionales.isEmpty();
	}
	
}
