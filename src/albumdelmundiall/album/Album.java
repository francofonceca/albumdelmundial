package src.albumdelmundiall.album;

import java.util.Map;
import java.util.Random;
import src.albumdelmundiall.figurita.*;


public abstract class Album {
	private int codigo;
	private String tipoAlbum;
	private Map<String, Seleccion> selecciones;
	private String premio;

	public Album(String tipo, String premio) {
		this.codigo = getRandomCode();
		this.tipoAlbum = tipo;
		this.premio = premio;
	}

	public boolean seCompletoAlbum() {

		for (Map.Entry<String, Seleccion> seleccion : selecciones.entrySet()) {
			
			if(!seleccion.getValue().estaCompletoPais())
				return false;
		}
		
		return true;
	}
	
	public boolean sePegoFigurita(FiguritaTradicional figurita) { 
		Seleccion seleccion = selecciones.get(figurita.obtenerPais());
		return seleccion.sePegoFigPais(figurita);
	}

	public void pegarFigurita(FiguritaTradicional figu) {
		selecciones.get(figu.obtenerPais()).pegarFigPais(figu);
	}

	public boolean seCompletoSeleccion(String seleccion) {
		
		if (!selecciones.containsKey(seleccion)) {
			return false;
		}
		
		return selecciones.get(seleccion).estaCompletoPais();
	}

	public void obtenerSelecciones(Map<String, Seleccion> selecs) {
		selecciones = selecs;
	}
	
	private Integer getRandomCode() {
		
        Random random = new Random();
        return random.nextInt();
	}
	
	public String verTipoAlbum() {
		return tipoAlbum;
	}

	public String obtenerPremio() {
		return premio;
	}
	
	public int obtenerCodigo() {
		return codigo;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("Tipo de album " + tipoAlbum);
		sb.append("--------------------------");
		sb.append("Selecciones: ");
		
		for(Seleccion pais : selecciones.values()) {
			sb.append(pais);
			sb.append(" - ");
		}
		
		return sb.toString();
		
	}
}