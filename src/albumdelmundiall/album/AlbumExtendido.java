package src.albumdelmundiall.album;

import java.util.HashMap;
import java.util.Map;

import src.albumdelmundiall.figurita.*;

public class AlbumExtendido extends Album {

	public Map<Integer, FiguritaTop10> figuritasTop10;

	public AlbumExtendido(String tipoAlbum) {
		super(tipoAlbum, "pelota y viaje");
		this.figuritasTop10 = new HashMap<>();
	}

	public boolean sePegoFiguritaTop10(FiguritaTop10 figurita) {

		return figuritasTop10.containsValue(figurita);
	}

	public void pegarFiguritaTop10(FiguritaTop10 figurita) {
		
		if (!figuritasTop10.containsValue(figurita))
			figuritasTop10.put( figurita.getCodigo(),figurita);

	}

	public boolean seCompletoAlbum() {
		return super.seCompletoAlbum();
	}

}
