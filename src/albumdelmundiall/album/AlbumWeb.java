package src.albumdelmundiall.album;

public class AlbumWeb extends Album {
    private Boolean utilizado;

    public AlbumWeb(String tipoAlbum) {
    	super(tipoAlbum, "camiseta de seleccion");
        this.utilizado = false;
    }

    public Boolean utilizoCodigo() {
    	return utilizado;
    }
}
