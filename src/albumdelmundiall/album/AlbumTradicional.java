package src.albumdelmundiall.album;

public class AlbumTradicional extends Album {

    private boolean sorteoPremio;

    public AlbumTradicional(String tipoAlbum) {
    	super(tipoAlbum, "pelota");
        this.sorteoPremio = false;
    }

    public void sortearPremio() {
    	if (this.sorteoPremio) {
    		throw new RuntimeException("Premio ya sorteado");
    	}
    	this.sorteoPremio = true;
    }
    
    public boolean seLeSorteoPremio() {
    	return this.sorteoPremio;
    }
}
