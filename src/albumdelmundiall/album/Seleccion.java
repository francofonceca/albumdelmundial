package src.albumdelmundiall.album;

import java.util.ArrayList;
import java.util.List;

import src.albumdelmundiall.figurita.*;


public class Seleccion {
	private String nombre;
	private Integer ranking;
	private List<FiguritaTradicional> figuritas;

	public Seleccion(String nombre, Integer ranking) {
		this.nombre = nombre;
		this.ranking = ranking;
		this.figuritas = new ArrayList<>();
	}
	
	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("Figuritas obtenidas: ");
		sb.append("\n");
		for(FiguritaTradicional f : figuritas) {
			sb.append(f);
			sb.append("\n");
		}
		return sb.toString();
	}

	public boolean sePegoFigPais(FiguritaTradicional figurita) {

		if (figurita == null) {
			return true;
		}
		if (figuritas.size() == 0) {
			return false;
		}

		return figuritas.contains(figurita);
	}

	public void pegarFigPais(FiguritaTradicional figurita) {
		if (figurita == null) {
			throw new RuntimeException("figurita nulla");
		}
		if (!sePegoFigPais(figurita)) {
			figuritas.add(figurita);
		}
	}
 
	public boolean estaCompletoPais() {
		return figuritas.size() == 12;
	}
	
	public String obtenerNombreSeleccion() {
		return nombre;
	}
}
