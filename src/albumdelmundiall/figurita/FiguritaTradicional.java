package src.albumdelmundiall.figurita;

public class FiguritaTradicional {
	private int cont;
	private Integer codigo;
	private String tipoFigurita;
	private int numJugador;
	private String nombrePais;

	public FiguritaTradicional(String tipo, int numJugador, String nombrePais) {
		this.tipoFigurita = tipo;
		this.numJugador = numJugador;
		this.nombrePais = nombrePais;
		generarCodigo((nombrePais.length() + numJugador + tipoFigurita.length())/3 + cont);
		cont++;
	}

	public void generarCodigo(int c) {
		codigo = c;
	}

	public int getCodigo() {
		return codigo;
	}

	public int getNumJugador() {
		return numJugador;
	}

	public String getTipo() {
		return tipoFigurita;
	}

	public String getNombrePais() {
		return nombrePais;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof FiguritaTradicional)) {
			return false;
		}

		FiguritaTradicional fig = (FiguritaTradicional) obj;

		return numJugador == fig.getNumJugador() && nombrePais.equals(fig.getNombrePais())
				&& tipoFigurita.equals(fig.getTipo());
	}
	
	public String obtenerPais() {
		return getNombrePais();
	}
}
