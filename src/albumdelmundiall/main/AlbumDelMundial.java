package src.albumdelmundiall.main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import src.albumdelmundiall.album.Album;
import src.albumdelmundiall.*;
import src.albumdelmundiall.figurita.FiguritaTop10;
import src.albumdelmundiall.figurita.FiguritaTradicional;
import src.albumdelmundiall.participante.Participante;

public class AlbumDelMundial implements IAlbumDelMundial {

	private Random random;
	private Map<Integer, Participante> participantes;
	private Fabrica fabrica;

	public AlbumDelMundial() {

		fabrica = new Fabrica();
		participantes = new HashMap<>();
		random = new Random();
	}

	@Override
	public int registrarParticipante(int dni, String nombre, String tipoAlbum) {

		if (participantes.containsKey(dni)) {
			throw new RuntimeException("Participante ya se encuentra registrado");

		} else if (!tipoAlbum.equals("Web") && !tipoAlbum.equals("Tradicional") && !tipoAlbum.equals("Extendido")) {
			throw new RuntimeException("Ha ingresado un tipo de álbum incorrecto");
		}
		if (dni < 0) {
			throw new RuntimeException("Ha ingresado un tipo de dni erróneo");
		}
		if (nombre == null)
			throw new RuntimeException("Ha ingresado un nombre inválido");

		Album album;

		if (tipoAlbum.equals("Web")) {
			album = fabrica.crearAlbumWeb();

		} else if (tipoAlbum.equals("Tradicional")) {

			album = fabrica.crearAlbumTradicional();
		} else {
			album = fabrica.crearAlbumExtendido();
		}

		album.obtenerSelecciones(fabrica.get_paises()); 

		Participante participante = new Participante(dni, album, nombre);

		participantes.put(participante.obtenerDNI(), participante);

		return album.obtenerCodigo();
	}

	@Override
	public void comprarFiguritas(int dni) {
		if (!participantes.containsKey(dni)) {
			throw new RuntimeException("No se encuentra registrado");
		}
		Participante participante = participantes.get(dni);

		participante.cargarSobreDeFiguritas(fabrica.generarSobre(4));
	}

	@Override
	public void comprarFiguritasTop10(int dni) {
		if (!participantes.containsKey(dni)) {
			throw new RuntimeException("No se encuentra registrado");

		} else if (!participantes.get(dni).obtenerTipoAlbum().equals("Extendido")) {
			throw new RuntimeException("El participante no cuenta con un Álbum Extendido");
		}

		Participante participante = participantes.get(dni);

		participante.cargarSobreDeFiguritas(fabrica.generarSobreTop10(4));

	}

	@Override
	public void comprarFiguritasConCodigoPromocional(int dni) {

		Participante participante = participantes.get(dni);

		participante.utilizoCodigo();
		participante.cargarSobreDeFiguritas(fabrica.generarSobre(4));

	}

	public List<String> pegarFiguritas(int dni) {
			
		List<String> fFinales = new ArrayList<>();
		participantes.get(dni).pegarFiguritas().forEach( (codigo, figu) -> {
			fFinales.add(figu.toString());
		});

		return fFinales;
	}

	@Override
	public boolean llenoAlbum(int dni) {
		if (!participantes.containsKey(dni)) {
			throw new RuntimeException("No se encuentra registrado");
		}

		Participante participante = participantes.get(dni);

		return participante.seCompletoAlbum();
	}

	@Override
	public String aplicarSorteoInstantaneo(int dni) {
		if (!participantes.containsKey(dni)) {
			throw new RuntimeException("No se encuentra registrado");

		} else if (!(participantes.get(dni).obtenerTipoAlbum().equals("Tradicional"))
				&& !(participantes.get(dni).obtenerTipoAlbum().equals("Extendido"))) {
			throw new RuntimeException("El participante no tiene un Álbum Tradicional");

		} else if (participantes.get(dni).seUtilizoSorteo()) {
			throw new RuntimeException("El participante no tiene disponible el sorteo");
		}

		Participante participante = participantes.get(dni);
		int numSorteo = random.nextInt(3);
		participante.usarSorteo();
		return fabrica.sortearPremio(numSorteo);
	}

	@Override
	public int buscarFiguritaRepetida(int dni) {
		if (!participantes.containsKey(dni)) {
			throw new RuntimeException("No se encuentra registrado");
		}

		Participante participante = participantes.get(dni);
		return participante.darCodigoFiguRepetida();
	}

	@Override
	public boolean intercambiar(int dni, int codFigurita) {
		if (!participantes.containsKey(dni)) {
			throw new RuntimeException("No se encuentra registrado");
		}

		if (codFigurita < 1) {
			return false;
		}

		if (!participantes.get(dni).tieneEsaFigurita(codFigurita)) {
			throw new RuntimeException("No posee la figurita");
		}

		Participante participante = participantes.get(dni);
		FiguritaTradicional figurita = participante.obtenerFiguritaTradicional(codFigurita);

		if (figurita == null) {
			return false;
		}

		for (Map.Entry<Integer, Participante> otroP : participantes.entrySet()) {

			if (!participante.equals(otroP.getValue())) {
				
				if (participante.obtenerTipoAlbum().equals(otroP.getValue().obtenerTipoAlbum())) {

					int otroCod = buscarFiguritaRepetida(otroP.getKey());

					if (otroCod != -1 && !participante.tieneEsaFigurita(otroCod)) {

						FiguritaTradicional otraFigurita = otroP.getValue().obtenerFiguritaTradicional(otroCod);

						if (!figurita.equals(otraFigurita) && mismoOMenorValor(figurita, otraFigurita)) {

							participante.intercambiar(figurita, otraFigurita);

							otroP.getValue().intercambiar(otraFigurita, figurita);

							return true;
						}
					}
				}
			}
		}
		return true;
	}

	private boolean mismoOMenorValor(FiguritaTradicional figurita, FiguritaTradicional otraFigurita) {
		int val1 = fabrica.valorBase(figurita);
		int val2 = fabrica.valorBase(otraFigurita);

		if (figurita.getTipo().equals("top10")) {
			FiguritaTop10 f1 = (FiguritaTop10) figurita;
			val1 *= f1.queBalon().equals("oro") ? 1.20 : 1.10;
		}
		if (otraFigurita.getTipo().equals("top10")) {
			FiguritaTop10 f2 = (FiguritaTop10) otraFigurita;
			val1 *= f2.queBalon().equals("oro") ? 1.20 : 1.10;
		}
		return val1 >= val2;

	}

	@Override
	public boolean intercambiarUnaFiguritaRepetida(int dni) {
		if (!participantes.containsKey(dni)) {
			throw new RuntimeException("No se encuentra registrado");
		}

		int codFig = buscarFiguritaRepetida(dni);
		return intercambiar(dni, codFig);
	}

	@Override
	public String darNombre(int dni) {
		if (!participantes.containsKey(dni)) {
			throw new RuntimeException("No se encuentra registrado");
		}
		Participante participante = participantes.get(dni);
		return participante.obtenerNombre();
	}

	@Override
	public String darPremio(int dni) {
		if (!participantes.containsKey(dni)) {
			throw new RuntimeException("No se encuentra registrado");
		} else if (!participantes.get(dni).seCompletoAlbum()) {
			throw new RuntimeException("Todavia no completo el album");
		}

		Participante participante = participantes.get(dni);

		return participante.obtenerPremio();
	}

	@Override
	public String listadoDeGanadores() {
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<Integer, Participante> p : participantes.entrySet()) {
			if (p.getValue().seCompletoAlbum()) {
				sb.append(p.getValue().toString() + "\n");
			}
		}
		return sb.toString();
	}

	@Override
	public List<String> participantesQueCompletaronElPais(String nombrePais) {
		
		List<String> completaronPais = new ArrayList<>();
		for (Map.Entry<Integer, Participante> p : participantes.entrySet()) {
		
			if (p.getValue().seCompletoSeleccion(nombrePais)) {
				completaronPais.add(p.getValue().toString());
			}
		
		}
		
		return completaronPais;
	}
	
	@Override 
	public String toString() {
		
		StringBuilder sb = new StringBuilder();
		
		int count = 0;
		sb.append("Participantes totales ");
		
		sb.append(participantes.size());
		sb.append("-------------------");
		sb.append("Ganadores totales: ");
		for (Participante  p : participantes.values()) {
			if (p.seCompletoAlbum()) {
				count++;
			}
		}
		sb.append(count);
		
		return sb.toString();
	}
}
